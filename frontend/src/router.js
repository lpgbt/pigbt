import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/core",
      name: "core",
      component: () => import("./views/Core.vue"),
    },
    {
      path: "/status",
      name: "status",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Status.vue"),
    },
    {
      path: "/i2cm",
      name: "i2cm",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/I2CMaster.vue"),
    },
    {
      path: "/clocks",
      name: "clocks",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Clocks.vue"),
    },
    {
      path: "/gpio",
      name: "gpio",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Gpio.vue"),
    },
    {
      path: "/highspeed",
      name: "highspeed",
      component: () => import("./views/HighSpeed.vue"),
    },
    {
      path: "/analog",
      name: "analog",
      component: () => import("./views/Analog.vue"),
    },
    {
      path: "/testfeatures",
      name: "testfeatures",
      component: () => import("./views/TestFeatures.vue"),
    },
    {
      path: "/regmap",
      name: "regmap",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/RegMap.vue"),
    },
    {
      path: "/connection",
      name: "connection",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Connection.vue"),
    },
    {
      path: "/testoutputs",
      name: "testoutputs",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/TestOutputs.vue"),
    },
    {
      path: "/about",
      name: "about",
      component: () => import("./views/About.vue"),
    },
  ],
});
