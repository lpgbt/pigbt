import '@babel/polyfill'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import './plugins/bootstrap-vue'
import './registerServiceWorker'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faBrain, faMap, faUsers, faLink, faArrowsAltH,
  faDatabase, faEye, faTachometerAlt, faBookOpen, faCogs, faToggleOn, faToggleOff } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import bFormSlider from 'vue-bootstrap-slider'
import 'bootstrap-slider/dist/css/bootstrap-slider.css'

import 'v-slim-dialog/dist/v-slim-dialog.css'
import SlimDialog from 'v-slim-dialog'
import './assets/style.css'

Vue.component('icon', Icon)

Vue.use(bFormSlider)

Vue.use(SlimDialog)

library.add(faCoffee)
library.add(faMap)
library.add(faBrain)
library.add(faUsers)
library.add(faLink)
library.add(faArrowsAltH)
library.add(faDatabase)
library.add(faEye)
library.add(faTachometerAlt)
library.add(faBookOpen)
library.add(faCogs)
library.add(faToggleOn)
library.add(faToggleOff)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

Vue.prototype.$http = axios

export const globalStore = new Vue({
  data: {
    state: 'locked'
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
