// vue.config.js
module.exports = {
    devServer: {
        host: '0.0.0.0',
        port: '8080',
        public: 'lpGBT-api.com',
    },
    chainWebpack: config => {
        config.module.rules.delete('eslint');
    },
}
