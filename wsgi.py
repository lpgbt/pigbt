# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import Response
from flask import url_for
from flask import make_response
from flask import send_file
import logging
from flask_menu import Menu, register_menu
from flask import jsonify
from flask_cors import CORS

from gevent import pywsgi

import os

#Initialise FLASK Application
application = Flask(__name__)
application.secret_key = 'jh345yh0wgn5o9t39t43j4jg'

@application.route('/')
def index():
    return send_file('./frontend/dist/index.html')

def getListOfFiles(dirName):
    # create a list of file and sub directories
    # names in the given directory
    try:
        listOfFile = os.listdir(dirName)
    except:
        return []

    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)

    return allFiles

def redirect_static():
    route = request.url_rule
    return send_file('./frontend/dist{}'.format(route))

for file in getListOfFiles('./frontend/dist'):
    file = file.replace('./frontend/dist/','')
    print('Create route for: {}'.format(file))
    application.add_url_rule('/'+file, 'redirect_static', redirect_static)

logging.basicConfig(format='[%(levelname)-7s] %(message)s')
logging.getLogger().setLevel(logging.INFO)

from backend.apiapp import api
application.register_blueprint(api.bp)
CORS(application)

if __name__ == "__main__":
    server = pywsgi.WSGIServer(('0.0.0.0', 5000), application)
    server.serve_forever()
