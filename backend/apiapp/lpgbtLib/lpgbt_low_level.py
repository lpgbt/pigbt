import os
import logging

from .lowLevelDrivers.UHALGPIO import GPIO as UHALgpio
from .lowLevelDrivers.UHALI2C import I2C as UHALi2c
from .lowLevelDrivers.RPIGPIO import GPIO as RPIgpio
from .lowLevelDrivers.RPII2C import I2C as RPIi2c
from .lowLevelDrivers.MOCKUPGPIO import GPIO as MOCKUPgpio
from .lowLevelDrivers.MOCKUPI2C import I2C as MOCKUPi2c
from .lowLevelDrivers.MASTERGPIO import GPIO as MASTERgpio
from .lowLevelDrivers.MASTERI2C import I2C as MASTERi2c


class LpgbtLowLevel:

    UHAL = 0
    RPI = 1
    MOCKUP = 2
    LPGBTMASTER = 3

    PLATFORM_NAME = [
        'UHAL',
        'RPI',
        'MOCKUP',
        'LPGBTMASTER'
    ]

    LPGBT_LED1_GREEN = 0
    LPGBT_LED1_RED = 1
    APP_LED2_RED = 2
    CTRL_TSTOUT3 = 3

    CTRL_MODE0 = 4
    CTRL_MODE1 = 5
    CTRL_MODE2 = 6
    CTRL_MODE3 = 7

    CTRL_ADDR0 = 8
    CTRL_ADDR1 = 9
    CTRL_ADDR2 = 10
    CTRL_ADDR3 = 11

    CTRL_RSTB = 12
    CTRL_FUSE0 = 13
    CTRL_FUSE1 = 14
    CTRL_STATEOVRD = 15
    CTRL_PORDIS = 16
    CTRL_VCOBYPASS = 17
    CTRL_READY = 18
    CTRL_RSTOUTB = 19

    CTRL_PINS = [
        [0, 1, 2, 3, 9, 10, 11, 12, 5, 6, 7, 8, 4, 13,
            14, 15, 16, 17, 18, 19],  # UHAL platform
        [14, 15, 23, -1, 13, 6, 5, 11, -1, -1, -1, -1, 9,
            27, 22, -1, 19, -1, -1, -1],  # RPI platform
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 27,
         22, 15, 16, 17, 18, 19],  # Mock-up platform
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
         15, 16, 17, 18, 19],  # LpGBT Master platform
    ]

    def to_dict(self):
        dict = {
            'platform': self.platform,
            'i2caddr': self.i2c_addr,
            'options': self.options,
            'chip': self.chip_version,
            'uhali2c': self.uhali2c.to_dict(),
            'uhalgpio': self.uhalgpio.to_dict(),
            'rpii2c': self.rpii2c.to_dict(),
            'rpigpio': self.rpigpio.to_dict(),
            'mockupi2c': self.mockupi2c.to_dict(),
            'mockupgpio': self.mockupgpio.to_dict(),
            'masteri2c': self.masteri2c.to_dict(),
            'mastergpio': self.mastergpio.to_dict()
        }

        return dict

    def from_dict(self, dict):
        self.platform = dict['platform']
        self.chip_version = dict['chip']

        try:
            self.i2c_addr = dict['i2caddr']
        except:
            self.i2c_addr = None

        try:
            self.options = dict['options']
        except:
            self.options = None

        self.uhali2c.from_dict(dict['uhali2c'])
        self.uhalgpio.from_dict(dict['uhalgpio'])
        self.rpii2c.from_dict(dict['rpii2c'])
        self.rpigpio.from_dict(dict['rpigpio'])
        self.mockupi2c.from_dict(dict['mockupi2c'])
        self.mockupgpio.from_dict(dict['mockupgpio'])
        # self.masteri2c.from_dict(dict['masteri2c']) # Nothing to do
        try:
            self.mastergpio.from_dict(dict['mastergpio'])
        except:
            self.mastergpio.from_dict({})

        self.set_platform(self.platform, self.options)

    def __init__(self, platform=2, gpio_uhal_driver=None, i2c_uhal_driver=None):

        self.supported_platforms = [self.LPGBTMASTER, self.MOCKUP]
        self.platform = platform
        self.i2c_addr = None
        self.chip_version = None

        # check platforms
        try:
            import RPi.GPIO
            self.supported_platforms.append(self.RPI)
            self.supported_platforms.append(self.LPGBTMASTER)
        except Exception as e:
            logging.warning('RPI Platform is not supported')

        if gpio_uhal_driver is not None and i2c_uhal_driver is not None:
            self.supported_platforms.append(self.UHAL)
        else:
            logging.warning('UHAL Platform is not supported')

        if platform not in self.supported_platforms:
            raise Exception('Selected platform ({}) is not supported'.format(
                self.PLATFORM_NAME[platform]))

        self.uhali2c = UHALi2c()
        self.uhalgpio = UHALgpio()
        self.rpii2c = RPIi2c()
        self.rpigpio = RPIgpio()
        self.mockupi2c = MOCKUPi2c(self)
        self.mockupgpio = MOCKUPgpio(self)
        self.masteri2c = MASTERi2c(self)
        self.mastergpio = MASTERgpio(self)

        # Init GPIO and I2C interfaces
        if platform == self.UHAL:
            self.gpio = self.uhalgpio
            self.i2c = self.uhali2c
        elif platform == self.RPI:
            self.gpio = self.rpigpio
            self.i2c = self.rpii2c
        elif platform == self.MOCKUP:
            self.gpio = self.mockupgpio
            self.i2c = self.mockupi2c
        elif platform == self.LPGBTMASTER:
            self.gpio = self.mastergpio
            self.i2c = self.masteri2c

    def get_i2c(self):
        return self.i2c

    def get_gpio(self):
        return self.gpio

    def set_platform(self, platform, options=None):
        if platform not in self.supported_platforms:
            raise Exception('Selected platform ({}) is not supported'.format(
                self.PLATFORM_NAME[platform]))

        if platform == self.UHAL:
            self.gpio = self.uhalgpio
            self.i2c = self.uhali2c
        elif platform == self.RPI:
            self.gpio = self.rpigpio
            self.i2c = self.rpii2c
        elif platform == self.MOCKUP:
            self.gpio = self.mockupgpio
            self.i2c = self.mockupi2c
        elif platform == self.LPGBTMASTER:
            self.gpio = self.mastergpio
            self.i2c = self.masteri2c

        self.options = options
        self.platform = platform

    def set_i2c_address(self, i2c_addr):
        self.i2c_addr = i2c_addr

    def get_i2c_address(self):
        # if self.i2c_addr is None:
        #raise Exception("No i2c address selected")
        return self.i2c_addr

    def get_platform(self):
        return self.platform

    def set_chip_version(self, chip_version):
        self.chip_version = chip_version

    def get_chip_version(self):
        return self.chip_version

    def set_pin(self, pin, value=True):
        # Check whether multiple pin or only one is controlled
        if isinstance(self.CTRL_PINS[self.platform][pin], list):
            for pin_id in self.CTRL_PINS[self.platform][pin]:
                self.gpio.set_pin(pin_id, value)
            return

        # Check whether the pin exists or not
        if self.CTRL_PINS[self.platform][pin] == -1:
            raise Exception(
                'Pin not supported on the selected platform ({})'.format(self.platform))

        self.gpio.set_pin(self.CTRL_PINS[self.platform][pin], value)

    def get_pin(self, pin):
        # Check whether multiple pin or only one is controlled
        #  -> in this case, all pins should have the same value. Therefore only the first one is returned
        if isinstance(self.CTRL_PINS[self.platform][pin], list):
            return self.gpio.get_pin(self.CTRL_PINS[self.platform][pin][0])

        # Check whether the pin exists or not
        if self.CTRL_PINS[self.platform][pin] == -1:
            raise Exception(
                'Pin not supported on the selected platform ({})'.format(self.platform))

        return self.gpio.get_pin(self.CTRL_PINS[self.platform][pin])

    def write_byte(self, addr, value):
        self.i2c.write_byte(addr, value)

    def write_i2c_block_data(self, addr, regl, t_values):
        self.i2c.write_i2c_block_data(addr, regl, t_values)

    def read_byte(self, addr):
        return self.i2c.read_byte(addr)

    def read_i2c_block_data(self, offset, registeradd, len):
        return self.i2c.read_i2c_block_data(offset, registeradd, len)

    def lpgbt_reads(self, device_addr, reg_addr, read_len):
        fnargs = {
            'device_addr': device_addr,
            'reg_addr': reg_addr,
            'read_len': read_len
        }

        if self.options is not None:
            fnargs.update(self.options)

        return self.i2c.lpgbt_reads(**fnargs)

    def lpgbt_write(self, device_addr, reg_addr, reg_vals):
        fnargs = {
            'device_addr': device_addr,
            'reg_addr': reg_addr,
            'reg_vals': reg_vals
        }

        if self.options is not None:
            fnargs.update(self.options)

        return self.i2c.lpgbt_write(**fnargs)

    def run_emulator(self):
        if self.platform != self.MOCKUP:
            return

        ctrl0_value = self.get_pin(self.CTRL_MODE0)
        ctrl1_value = self.get_pin(self.CTRL_MODE1)
        ctrl2_value = self.get_pin(self.CTRL_MODE2)
        ctrl3_value = self.get_pin(self.CTRL_MODE3)

        reg = ctrl0_value & 0x01
        reg |= (ctrl1_value & 0x01) << 1
        reg |= (ctrl2_value & 0x01) << 2
        reg |= (ctrl3_value & 0x01) << 3

        if self.chip_version == 0:
            self.lpgbt_write(0, 0x0140, [(reg << 4)])
            self.lpgbt_write(0, 0x1c5, [0xa5])
        if self.chip_version == 1:
            self.lpgbt_write(0, 0x0150, [(reg << 4)])
            self.lpgbt_write(0, 0x1d7, [0xa6])
