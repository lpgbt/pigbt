import logging

try:
    import RPi.GPIO as gpio
    RPISupported = True
except Exception as e:
    RPISupported = False
    logging.warning('RPi.GPIO import: {}'.format(e))

class GPIO:

    def __init__(self):
        if RPISupported:
            gpio.setwarnings(False)
            gpio.setmode(gpio.BCM)

    def set_pin(self, gpio_pin, value):

        if RPISupported == False:
            raise Exception('RPI is not supported')

        gpio.setup(gpio_pin,gpio.OUT)
        gpio.output(gpio_pin,value)

    def get_pin(self, gpio_pin):

        if RPISupported == False:
            raise Exception('RPI is not supported')

        gpio.setup(gpio_pin,gpio.OUT)
        return gpio.input(gpio_pin)

    def to_dict(self):
        return {}

    def from_dict(self, dict):
        logging.debug('Nothing to do...')