import logging
import os
import uuid
from collections import defaultdict


class I2C:

    def __init__(self, lpgbtLowLevelInst):
        self.devices = {}
        self.lpgbtLowLevelInst = lpgbtLowLevelInst
        self.filename = str(uuid.uuid4())  # Random name
        self.registers = {}

    def write_byte(self, addr, value, reg=-1):
        dummy = ''

    def read_byte(self, addr, reg=-1):
        return 0

    def write_i2c_block_data(self, addr, regl, t_values):
        return 0

    def read_i2c_block_data(self, addr, registeradd, len):
        bytes = []

        for i in range(len):
            bytes.append(self.lpgbt_read(addr, registeradd))
            registeradd += 1

        return bytes

    def lpgbt_write(self, device_addr, reg_addr, reg_vals):
        for i, value in enumerate(reg_vals):
            self.registers['{}'.format(reg_addr+i)] = value

    def lpgbt_reads(self, device_addr, reg_addr, read_len):
        reg_values = []
        if '{}'.format(reg_addr) in self.registers:
            for i in range(read_len):
                reg_values.append(self.registers['{}'.format(reg_addr+i)])
            return reg_values
        else:
            data = [0]
            return data

    def to_dict(self):
        return {
            'devices': self.devices,
            'register': self.registers,
            'filename': self.filename
        }

    def from_dict(self, dict):
        self.devices = dict['devices']
        self.filename = dict['filename']
        self.registers = dict['register']
