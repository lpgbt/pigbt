from flask import session
from flask import jsonify

from . lpgbtLib.lpgbt_low_level import LpgbtLowLevel
from . lpgbtLib.pigbt import PiGBT

import uuid
import pickle
from gevent.lock import Semaphore
import traceback

from threading import Thread

fileaccess_lock = Semaphore(1)
gevent_lock = Semaphore(1)

def write_result(key, result):
    fileaccess_lock.acquire()

    try:
        with open(r"./asynch_result.pickle", "rb") as input_file:
            e = pickle.load(input_file)
    except:
        e = {}

    e[key] = result

    with open(r"./asynch_result.pickle", "wb") as output_file:
        pickle.dump(e, output_file)

    fileaccess_lock.release()

def write_progress(key, progress):
    fileaccess_lock.acquire()

    try:
        with open(r"./asynch_progress.pickle", "rb") as input_file:
            e = pickle.load(input_file)
    except:
        e = {}

    e[key] = progress

    with open(r"./asynch_progress.pickle", "wb") as output_file:
        pickle.dump(e, output_file)

    fileaccess_lock.release()

def read_progress(key):
    try:
        with open(r"./asynch_progress.pickle", "rb") as input_file:
            e = pickle.load(input_file)
    except:
        e = {}

    if key in e:
        return e[key]

    return None

def read_result(key):
    try:
        with open(r"./asynch_result.pickle", "rb") as input_file:
            e = pickle.load(input_file)
    except:
        e = {}

    if key in e:
        return e[key]

    return None

def pop_result(key):
    fileaccess_lock.acquire()

    try:
        with open(r"./asynch_result.pickle", "rb") as input_file:
            e = pickle.load(input_file)
    except:
        e = {}

    try:
        tmp = e.pop(key)
    except:
        tmp = None

    with open(r"./asynch_result.pickle", "wb") as output_file:
        pickle.dump(e, output_file)

    fileaccess_lock.release()
    return tmp

def pop_progress(key):
    fileaccess_lock.acquire()

    try:
        with open(r"./asynch_progress.pickle", "rb") as input_file:
            e = pickle.load(input_file)
    except:
        e = {}

    try:
        tmp = e.pop(key)
    except:
        tmp = None

    with open(r"./asynch_progress.pickle", "wb") as output_file:
        pickle.dump(e, output_file)

    fileaccess_lock.release()
    return tmp

def synch(function):
    def synch_wrapper():

        if 'progressKey' in session:  
            progress = read_progress(session['progressKey'])
            result = read_result(session['progressKey'])

            if progress is not None:
                return jsonify({'status':'failed', 'result': {'code': -1, 'message': 'Asynch function is already running'}})

            if result is not None:
                session['lpgbtLowLevel'] = result['lpgbtLowLevel']
                result['updated'] = True
                write_result(session['progressKey'], result)

        lpgbtLowLevel = LpgbtLowLevel(platform=LpgbtLowLevel.MOCKUP)
        if 'lpgbtLowLevel' in session and session['lpgbtLowLevel'] is not None:
            print(session['lpgbtLowLevel'])
            lpgbtLowLevel.from_dict(session['lpgbtLowLevel'])
        else:
            try:
                lpgbtLowLevel.set_platform(LpgbtLowLevel.RPI)
            except:
                print('RPI Platform is not supported - Stay on Mock-up')
        pigbtInst = PiGBT(lpgbtLowLevel)
        lpgbtInst = pigbtInst.lpgbt
        
        try:
            result = function(lpgbtInst)
            status = 'success'
        except Exception as excpt:
            status = 'failed'
            result = {'message': str(excpt), 'code': -2}

            print(traceback.format_exc())

        pigbtInst.run_emulator()

        session['lpgbtLowLevel'] = pigbtInst.getLpgbtLowLevelInst().to_dict()
        print(session['lpgbtLowLevel'])

        return jsonify({'status':status, 'result': result})

    return synch_wrapper
    
def synch_pigbt(function):
    def synch_wrapper():

        if 'progressKey' in session:
            progress = read_progress(session['progressKey'])
            result = read_result(session['progressKey'])

            if progress is not None:
                return jsonify({'status':'failed', 'result': {'code': -1, 'message': 'Asynch function is already running'}})

            if result is not None:
                session['lpgbtLowLevel'] = result['lpgbtLowLevel']
                result['updated'] = True
                write_result(session['progressKey'], result)

        lpgbtLowLevel = LpgbtLowLevel(platform=LpgbtLowLevel.MOCKUP)
        if 'lpgbtLowLevel' in session and session['lpgbtLowLevel'] is not None:
            print(session['lpgbtLowLevel'])
            lpgbtLowLevel.from_dict(session['lpgbtLowLevel'])
        else:
            try:
                lpgbtLowLevel.set_platform(LpgbtLowLevel.RPI)
            except:
                print('RPI Platform is not supported - Stay on Mock-up')
        lpgbtInst = PiGBT(lpgbtLowLevel)

        try:
            result = function(lpgbtInst)
            status = 'success'
        except Exception as excpt:
            status = 'failed'
            result = {'message': str(excpt), 'code': -2}

            print(traceback.format_exc())

        lpgbtInst.run_emulator()

        session['lpgbtLowLevel'] = lpgbtInst.getLpgbtLowLevelInst().to_dict()
        print(session['lpgbtLowLevel'])

        return jsonify({'status':status, 'result': result})

    return synch_wrapper    

def asynch(function):
    def asynch_wrapper():

        gevent_lock.acquire()

        if 'progressKey' in session:
            progress = read_progress(session['progressKey'])
            result = read_result(session['progressKey'])

            if progress is not None:
                gevent_lock.release()
                return jsonify({'status':'failed', 'result': {'code': -1, 'message': 'Asynch function is already running'}})

            if result is not None:
                session['lpgbtLowLevel'] = result['lpgbtLowLevel']
                result['updated'] = True
                write_result(session['progressKey'], result)

        session['functionname'] = str(function.__name__)
        progressKey = uuid.uuid1()
        write_progress(progressKey, 0)
        session['progressKey'] = progressKey


        def run_asynch_function(function, lpgbtInst, progressKey):

            try:
                func = function(lpgbtInst)

                while True:
                    try:
                        write_progress(progressKey, int(next(func)))
                    except StopIteration as e:
                        write_result(progressKey, {
                                'status': 'success',
                                'result': e.value,
                                'lpgbtLowLevel': lpgbtInst.getLpgbtLowLevelInst().to_dict(),
                                'updated': False
                            })
                        break

            except Exception as excpt:
                write_result(progressKey, {
                            'status': 'failed',
                            'result': {'message': str(excpt), 'code': -2},
                            'lpgbtLowLevel': lpgbtInst.getLpgbtLowLevelInst().to_dict(),
                            'updated': False
                        })

            finally:
                pop_progress(progressKey)

        lpgbtLowLevel = LpgbtLowLevel(platform=LpgbtLowLevel.MOCKUP)
        if 'lpgbtLowLevel' in session and session['lpgbtLowLevel'] is not None:
            lpgbtLowLevel.from_dict(session['lpgbtLowLevel'])
        else:
            try:
                lpgbtLowLevel.set_platform(LpgbtLowLevel.RPI)
            except:
                print('RPI Platform is not supported - Stay on Mock-up')
        lpgbtInst = PiGBT(lpgbtLowLevel)

        thread = Thread(target = run_asynch_function, args = (function, lpgbtInst, progressKey))
        thread.start()

        gevent_lock.release()

        return jsonify({'status':'run', 'result': {'function_name': str(function.__name__), 'progress': 0}})

    return asynch_wrapper

def get_asynch_status():

    gevent_lock.acquire()
    progress = read_progress(session['progressKey'])
    result = read_result(session['progressKey'])

    if progress is not None:
        gevent_lock.release()
        return jsonify({'status':'run','result': {'function_name': session['functionname'], 'progress': progress}})

    elif result is not None:
        res = pop_result(session['progressKey'])

        if res is None:
            gevent_lock.release()
            return jsonify({'status':'failed','result': {'message': 'Server error: file could have been deleted during execution', 'code': -3}})

        if res['updated'] == False:
            session['lpgbtLowLevel'] = res['lpgbtLowLevel']
        gevent_lock.release()
        return jsonify({'status':res['status'], 'result': res['result']})

    gevent_lock.release()
    return jsonify({'status':'failed','result': {'message': 'Nothing to do...', 'code': -1}})

def init(bp):
    bp.add_url_rule('/get_asynch_status', 'get_asynch_status', get_asynch_status)
