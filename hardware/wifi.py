import random
import string
import subprocess
import os
import time

HOSTAPD_CONF = '/etc/hostapd/hostapd.conf'
SSID_LINE = '3p;41q'
PASSWORD_LINE = '11p;41q'

class wifi():

    def randomWiFiSSID(self):
        """Generate a random wifi SSID"""
        lettersAndDigits = string.digits
        ssid = 'ssid='+"PIGBT_"+''.join(random.choice(lettersAndDigits) for i in range(4))
        return ssid

    def randomWiFiPassword(self):
        """Generate a random wifi password """
        lettersAndDigits = string.ascii_letters  + string.digits
        password = 'wpa_passphrase='+''.join(random.choice(lettersAndDigits) for i in range(8))
        return password

    def update_param(self,old,new):
        args = 'sudo ' + 'sed ' + '-i ' + '-e ' + '"s/'+old+'/'+new+'/g" ' + HOSTAPD_CONF
        cmd = "".join(w.rstrip("\n") for w in args)
        subprocess.call(cmd, shell=True)

    def update_wifi(self):
        os.system('sudo systemctl stop hostapd')
        time.sleep(3)
        old_ssid = (subprocess.check_output(['sudo', 'sed', '-n', SSID_LINE , HOSTAPD_CONF ])).decode()
        new_ssid = self.randomWiFiSSID()
        self.update_param(old_ssid,new_ssid)

        old_psw = (subprocess.check_output(['sudo', 'sed', '-n', PASSWORD_LINE, HOSTAPD_CONF ])).decode()
        new_psw = self.randomWiFiPassword()
        self.update_param(old_psw,new_psw)
        
        os.system('sudo systemctl start hostapd')
